Title: Testing GitLab pages with Pelican
Date: 2018-04-22 11:45
Category: Testing

Following https://gitlab.com/pages/pelican

* Forked the project
* Remove fork relationship in:
    Settings => General => Advanced settings => Remove fork relationship
* Install pelican (in a virtualenv).
* Do not use `pelican-quickstart`
* Try in local:
~~~bash
xdg-open http://localhost:8000
make html VERBOSE=1 && make serve VERBOSE=1 &
~~~
* See the state of the pipeline
* See Settings => Pages
